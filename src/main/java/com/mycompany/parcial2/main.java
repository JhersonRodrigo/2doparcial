/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.parcial2;
import java.util.*;
/**
 *
 * @author Jherson Rodrigo Mamani Poma  CI:10577476
 */
public class main {
    public static void main( String args[] ){
       Scanner leer = new Scanner(System.in);
      
       colagenerica obj = new colagenerica();
      
       int op;
       int num;
      
       do{
          menu();
          op = leer.nextInt();
         
          switch(op){
              case 1:
                     System.out.println( "Numero a insertar" );
                     num = leer.nextInt();
                     if(obj.inscola(num)){
                        
                        System.out.println( "El numero "+num+" se inserto en la cola ["+obj.dret+"]" );
                        System.out.println();
                     }
                     else{
                          System.out.println( "Cola llena" );
                     }
                     break;
              case 2:
                    if(obj.retcola()){
                       System.out.println( "El dato retirado fue: "+obj.dret );
                    }
                    else{
                        System.out.println( "Cola vacia" );
                    }
                    break;
              case 3:
                    if(obj.fre==-1 && obj.fin==-1){
                       System.out.println( "Cola vacia" );
                    }
                    else{
                         System.out.println( "Estado de la cola:" );
                         for(int i=obj.fre; i<=obj.fin; i++){
                            System.out.print(obj.c[i]+" \t");
                         }
                         break;
                    }
              case 4:
                  System.out.println("Elementos existentes: "+obj.size()+" - Espacios libres: "+ obj.free());
                  break;
              case 7:
                  System.out.println(obj.dret);

                  
          }
          
       }
       while(op != 7);
    }
   
    public static void menu(){    
       System.out.println( "\t Menu para colas \n" );
       System.out.println( "1.- Añadir" );
       System.out.println( "2.- Borrar" );
       System.out.println( "3.- Mostrar" );
       System.out.println( "4.- Numero de elementos" );
       System.out.println( "5.- Max y Min" );
       System.out.println( "6.- Buscar" );
       System.out.println( "7.- Sacar todo" );
       System.out.println( "n.- Fin" );
       System.out.println( "\n Selecciona" );
    }
}
